# Find module for libjalienO2.
# Variables:
#  - LIBJALIENO2_FOUND: TRUE if found
#  - LIBJALIENO2_LIBPATH: library path
#  - LIBJALIENO2_INCLUDE_DIR: include dir
include(FindPackageHandleStandardArgs)
find_library(LIBJALIENO2_LIBPATH "jalienO2"
             PATH_SUFFIXES "lib"
	     HINTS "${LIBJALIENO2}")
     find_path(LIBJALIENO2_INCLUDE_DIR "TJAlienContext.h"
          PATH_SUFFIXES "include"
          HINTS "${LIBJALIENO2}")
get_filename_component(LIBJALIENO2_LIBPATH ${LIBJALIENO2_LIBPATH} DIRECTORY)
find_package_handle_standard_args(LIBJALIENO2 DEFAULT_MSG
                                  LIBJALIENO2_LIBPATH LIBJALIENO2_INCLUDE_DIR)
include_directories(${LIBJALIENO2_INCLUDE_DIR})
link_directories(${LIBJALIENO2_LIBPATH})

get_filename_component(inc ${LIBJALIENO2_INCLUDE_DIR} ABSOLUTE)

if(LIBJALIENO2_FOUND AND NOT TARGET libjalien::libjalienO2)
  add_library(libjalien::libjalienO2 IMPORTED INTERFACE)
  set_target_properties(libjalien::libjalienO2
                        PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${inc})
endif()

unset(inc)

